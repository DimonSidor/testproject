<?php
/**
 * thirdtask.php code file.
 * User: Dmitry Sydorenko (sidorenko4er@gmail.com)
 * Date: 16.10.2017
 *
 * опишите что этот код делает и найдите ошибки если они есть
 *
 * По коду берутсяданные из EMAILS_TABLE в определенных статусах
 * формируется массив с данными для отправки писем и массив для дальнейшей обработки записей в EMAILS_TABLE
 * выбранные записи переводятся в processing статус EMAIL_STATUS_SENDING
 * идет попытка отправить их курлом в send_batch()
 * обрабатывается ответ и формируются два массива: успешные и неуспешные (для которых еще изменяется статус и добавляются записи в EMAILS_SPAMMED_TABLE, если нужно)
 * успешно отправленные писсьма (записи в EMAILS_TABLE) переводятся в статус EMAIL_STATUS_SENT
 * неуспешные просто выводятся для просмотра
 */
use vegcoders\core\db\DB;
# SETTINGS #
define('POSTMARK_TOKEN', 'some code');

// Не вижу откуда берутся все константы EMAILS_TABLE, ADMIN_EMAIL, EMAIL_STATUS_SENDING, EMAIL_STATUS_SENT, EMAILS_TABLE и т.д.
// Если они берутся из vegcoders\core\db\DB, то должны использоваться как DB::EMAILS_TABLE
// Но возможно они defined где то выше


# PREPARES #
// В запросах нежелательно использовать "*"
$select_sql = 'SELECT * FROM ' . EMAILS_TABLE . ' WHERE status IN (0,3) ORDER BY status ASC, crtime ASC LIMIT 10';
$prepared_mails = array(); // Sending format for API
$ids = array(); // For updating statuses


$to_send = DB::query($select_sql, 'array');
if ($to_send) { // Если я правильно понял, то DB::query() должно вернуть обязательно массив?
//я бы проверку сделал (is_array($to_send) && !empty($to_send), чтоб убрать все подозрения по поводу дальнейшей прогонки в foreach()
	$ids = array(); // Достаточно одного определения выше "For updating statuses"
	foreach ($to_send as $index => $mail) {
		// Здесь бы не помешала проверка, нельзя быть уверенным что эти ключи существуют
		// if(empty($mail['id']) || empty($mail['description'])) continue;
		$ids[$index] = $mail['id'];
		$desc = $mail['description'];
		$prepared_mails[] = array(
			'From' => ADMIN_EMAIL,
			'To' => $mail['to_email'],
			'Subject' => $mail['subject'],
			'HtmlBody' => $desc,
		);
	}
	// Если пустой $ids, то дальнейшее выполнение бессмыслено, даже при наличии проверок
	echo date('Y-m-d H:i:s') . ' STARTED EMAILS ' . implode(',', $ids) . PHP_EOL;

	# MAILING #
	change_statuses($ids, EMAIL_STATUS_SENDING);
	$return = send_batch($ids, $prepared_mails);
	if ($return['ok']) { // isset($return['ok']) && is_array($return['ok']) && !empty($return['ok'])
		change_statuses($return['ok'], EMAIL_STATUS_SENT);
	}
	echo date('Y-m-d H:i:s') . ' PENDING MAIL SENT ' . count($return['ok']) . ' / ERRORS ' . count($return['bad']) . PHP_EOL;
}

function change_statuses(array $ids, $new_status)
{
	// if (empty($ids) || !is_string($new_status)) return false; // не массив не пропустит а на пустоту не лишним проверить
	// и параметр new_status лучше бы проверить, перед тем как канкатенировать в запрос
	if (!$ids) return true;
	return DB::query('UPDATE ' . EMAILS_TABLE . ' SET status = ' . $new_status . ', chtime = NOW() WHERE id IN (' . implode(',', $ids) . ')');
}

function send_batch(array $ids, $emails)
{
	//$return = array('ok' => array(), 'bad' => array());
	//if(empty($ids) || empty($emails)) return $return;

	$ch = curl_init('https://api.postmarkapp.com/email/batch');
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($emails));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		'Accept: application/json',
		'Content-Type: application/json',
		'X-Postmark-Server-Token: ' . POSTMARK_TOKEN
	));
	$server_output = json_decode(curl_exec($ch), true);
	curl_close($ch);
	$return = array('ok' => array(), 'bad' => array()); // move up
	// Проверить что $server_output - array и непустой
	foreach($server_output AS $index => $mail) {
		// Нельзя быть уверенным что $ids[$index] существует (что мы получили ответ с полным соответствием ключей с нашим $ids)
		$db_id = $ids[$index]; // $db_id = (empty($ids[$index])) ? null : $ids[$index];
		// или $index вместо null, если планируется в дальнейшем использовать $return['bad'] кроме как в echo на 50 стр.

		// первые два условия взять в скобки и добавить && (!empty($db_id) || $db_id >= 0)
		if (!isset($mail['ErrorCode']) || !$mail['ErrorCode']) {
			$return['ok'][$db_id] = $db_id;
		} else {
			// опять же мы не уверены в существовании $mail['Message']
			// $message = (empty($mail['Message'])) ? '' : $mail['Message'];
			$message = $mail['Message'];
			$status = EMAIL_STATUS_FAILED;
			if (!(strpos($message, 'Error parsing')) === false) {
				$status = EMAIL_STATUS_WRONG_TO;
			} elseif (!(strpos($message, 'Found inactive addresses')) === false) {
				$status = EMAIL_STATUS_SPAMMED;
				// Так же не помешает проверить, что существует $emails[$index]['To']
				$to = $emails[$index]['To'];
				if(!DB::query('SELECT id FROM '. EMAILS_SPAMMED_TABLE . ' WHERE email='.DB::escape($to, true))) {
					DB::insert(EMAILS_SPAMMED_TABLE, array('email' => $to));
					echo 'ADDED TO SPAMMED ' . $to . PHP_EOL;
				}
			}
			$return['bad'][$db_id] = array(
				'status' => $status,
				'message' => $message
			);
		}
		// Я бы изменил так, но тяжело делать такие изменения не зная общей логики
		/*
		$db_id = (empty($ids[$index])) ? null : $ids[$index];
		if(isset($mail['ErrorCode']) && !$mail['ErrorCode'] || is_null($db_id))
		{
			$message = (empty($mail['Message'])) ? '' : $mail['Message'];
			$status = EMAIL_STATUS_FAILED;
			if(!(strpos($message, 'Error parsing')) === false) {
				$status = EMAIL_STATUS_WRONG_TO;
			} elseif (!(strpos($message, 'Found inactive addresses')) === false) {
				$status = EMAIL_STATUS_SPAMMED;
				// Так же не помешает проверить, что существует $emails[$index]['To']
				$to = $emails[$index]['To'];
				if(!DB::query('SELECT id FROM '. EMAILS_SPAMMED_TABLE . ' WHERE email='.DB::escape($to, true)))
				{
					DB::insert(EMAILS_SPAMMED_TABLE, array('email' => $to));
					echo 'ADDED TO SPAMMED ' . $to . PHP_EOL;
				}
			}
			$return['bad'][$db_id] = array(
				'status' => $status,
				'message' => $message
			);
			continue;
		}
		$return['ok'][$db_id] = $db_id;
		*/
	}
	return $return;
}