"# My project's README" 

 * Author: Dmytro Sydorenko (sidorenko4er@gmail.com)
 * Creation date - 2017-10-16
 * https://DimonSidor@bitbucket.org/DimonSidor/testproject.git
 
 Тестовое задание выполнял на Docker
 Не могу утверждать, что все правильно сделал, т.к. изучал Docker самостояельно 
 (буду рад любым замечаниям "что не так" и "как должно быть")
 Весь код для заданий в ветке master
 Для просмотра выполненого задания:
  * Настроить переадресацию Docker-а на http://testproject.example.com, в моем случае с 192.168.99.100
    (в файле C:\Windows\System32\drivers\ets\hosts добавить 
    "192.168.99.100  testproject.example.com" https://i.imgur.com/zXXTp3n.png)
  * Запустить Docker
  * Перейти в директорию со скачанным репозиторием
  * Запустить $ docker-compose up
  * Проверить название mysql container, должно быть 'testproject_mysql_1'
    если не совпадает (https://i.imgur.com/HcTY6Jw.png), нужно исправить используя volume php контейнера '/usr/src/app':
    исправить в classes/model/database.php:26 'const DB_HOST_DOCKER' на нужное значение
    на локальном VirtualHost исправить на '192.168.99.100:3306' (при запущенном mysql container)
    (с этим не хватило времени разобраться, хотел как то автоматически определять)
  * DB должна быть доступна на http://192.168.99.100:3306 (db: dev_test, user: testuser, pwd: 0000)
  * Исполнить дамп www-data/db_dump.sql в этой DB (еще не разобрался как сделать чтоб дамп автоматом разворачивался)
  * Первое задание должно быть доступно на http://testproject.example.com/
  * Второе задание должно быть доступно на http://testproject.example.com/secondtask.php
  * Тесты на второе задание https://i.imgur.com/BCTgDZN.png
  
  Закомител Docker files в ветку dockerfiles
  И все файлы проекта в ветку full_project
  
  * Переделанное задание в папке 2_classes_and_tests_new
  * Исполнить db_dump.sql
  * База настроена на пользователя root без пароля (для localhost),
    сменить при необходимости настройки подключения
  * Демонстрация доступна по localhost/sectask.php