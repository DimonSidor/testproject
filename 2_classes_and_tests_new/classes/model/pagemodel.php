<?php
/**
 * pagemodel.php code file.
 * User: Dmitry Sydorenko (sidorenko4er@gmail.com)
 * Date: 25.10.2017
 */

namespace Classes\Model;

use Classes\Model\DBMain;

class PageModel extends DBMain
{

	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Get all data from `pages` table for menu
	 * @return array
	 */
	public function getAllPageData():array
	{
		$data = array();

		// Only need fields friendly and title
		$sql = "SELECT friendly, title FROM pages;";
		$res = $this->execute($sql);
		if(empty($res['data']) || !is_array($res['data'])) return $data;

		return $res['data'];
	}

	/**
	 * Get data from `pages` table by filter
	 * @param string $whereField
	 * @param string $whereValue
	 * @return array
	 */
	public function getPageData($whereField = '', $whereValue = ''):array
	{
		$data = array();
		if(empty($whereField) || !isset($whereValue)) return $data;

		$whereField = htmlspecialchars(trim($whereField));
		$whereValue = htmlspecialchars(trim($whereValue));

		$sql = "SELECT id, friendly, title, description FROM pages WHERE $whereField = :$whereField;";

		$res = $this->execute($sql, array($whereField => $whereValue));
		if(empty($res['data']) || !is_array($res['data'])) return $data;

		return $res['data'];
	}

}