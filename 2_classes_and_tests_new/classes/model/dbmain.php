<?php
/**
 * dbmain.php code file.
 * User: Dmitry Sydorenko (sidorenko4er@gmail.com)
 * Date: 25.10.2017
 */

namespace Classes\Model;

use PDO,
	Exception;

class DBMain
{

	/**
	 * @var null|PDO
	 */
	protected $connection = null;

	/**
	 * @var string
	 */
	public $errorMsg = '';

	const DB_HOST = 'localhost';
	const DB_PORT = '3306';
	const DB_NAME = 'dev_test';
	const DB_USER = 'root';
	const DB_PASSWORD = '';

	function __construct()
	{
		if(is_null($this->connection))
		{
			$this->connect();
		}
	}

	protected function connect()
	{
		try
		{
			$this->connection = new PDO('mysql:host=' . self::DB_HOST . ';port= ' . self::DB_PORT .
				';dbname=' . self::DB_NAME . ';charset=utf8', self::DB_USER, self::DB_PASSWORD);
			$this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch (Exception $e)
		{
			$this->errorMsg .= "DB connection: Exception were catch. ";
		}
	}

	/**
	 * @param string $sql
	 * @param array $params
	 * @return array
	 */
	protected function execute($sql = '', $params = array()):array
	{
		$data = array();
		$return = array(
			'success' => true,
			'sql' => $sql,
			'data' => $data,
			'error' => ''
		);

		try
		{
			if(is_null($this->connection))
			{
				$this->errorMsg .= 'No DB connection. ';
				$return['error'] = $this->errorMsg;
				return $return;
			}
			if(empty($params))
			{
				$pdoStatement = $this->connection->query($sql);
				$result = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
			}
			elseif(is_array($params))
			{
				$pdoStatement = $this->connection->prepare($sql);
				foreach($params as $bind_key => $bind_value)
				{
					$pdoStatement->bindValue(":$bind_key", $bind_value);
				}
				$pdoStatement->execute();
				$result = $pdoStatement->fetchAll(PDO::FETCH_ASSOC);
			}

			if(empty($result)) return $return;

			$return['data'] = $result;
			return $return;
		}
		catch(Exception $e)
		{
			$this->errorMsg .= 'DB execution query: Exception were catch. ';
			// Report errors
			$return['success'] = false;
			$return['error'] = $this->errorMsg;
			return $return;
		}
	}

}