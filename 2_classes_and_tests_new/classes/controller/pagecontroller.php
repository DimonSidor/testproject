<?php
/**
 * pagecontroller.php code file.
 * User: Dmitry Sydorenko (sidorenko4er@gmail.com)
 * Date: 25.10.2017
 */

namespace Classes\Controller;

use \Exception,
	Classes\Model\PageModel;

class PageController
{

	/**
	 * @var PageModel|null
	 */
	public $pageModel = null;

	function __construct()
	{
		$this->pageModel = new PageModel();
	}

	/**
	 * Return json data for page by 'id' from GET
	 * @param int|null $pageIDParam
	 * @return string
	 */
	public function getPage(int $pageIDParam = null):string
	{
		$pageData = array(
			'success' => false,
			'data'    => array(),
			'error'   => '',
		);

		try
		{
			$get = $_GET;
			$pageID = (empty($get['id'])) ? $pageIDParam : $get['id'];

			if(is_null($pageID))
			{
				// Not exactly an error
				// If empty _GET can return all pages data (for example)
				// It depends from frontend logic, in this case need error message (for example 404) or something else
				$pageData['error'] = "Missed GET param 'id'";
				return json_encode($pageData);
			}

			$data = $this->pageModel->getPageData('id', $pageID);

			if(!empty($this->pageModel->errorMsg))
			{
				$pageData['error'] = $this->pageModel->errorMsg . '';
				return json_encode($pageData);
			}

			if(empty($data))
			{
				$pageData['error'] = "Page doesn't found by ID = $pageID";
				return json_encode($pageData);
			}
			$pageData['success'] = true;
			$pageData['data'] = $data;

			return json_encode($pageData);
		}
		catch (Exception $ex)
		{
			$pageData['error'] = $ex->getMessage();
			return json_encode($pageData);
		}
	}

	/**
	 * Get json data for all pages (now only for test menu)
	 * Not in task, can be deleted after testing
	 * @return string
	 */
	public function getPages():string
	{
		$pagesData = array(
			'success' => false,
			'data'    => array(),
			'error'   => '',
		);

		try
		{
			$data = $this->pageModel->getAllPageData();

			if(!empty($this->pageModel->errorMsg))
			{
				$pagesData['error'] = $this->pageModel->errorMsg;
				return json_encode($pagesData);
			}

			if(empty($data))
			{
				$pagesData['error'] = "No pages data in DB";
				return json_encode($pagesData);
			}
			$pagesData['success'] = true;
			$pagesData['data'] = $data;

			return json_encode($pagesData);
		}
		catch (Exception $ex)
		{
			$pagesData['error'] = $ex->getMessage();
			return json_encode($pagesData);
		}
	}

}