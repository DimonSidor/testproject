-- DB must be available on http://192.168.99.100:3306 (db: dev_test, user: testuser, pwd: 0000)

-- Create DB if not exists on localhost
CREATE DATABASE `dev_test` CHARACTER SET utf8 COLLATE utf8_general_ci;

-- Create table for pages
CREATE TABLE `pages` (
  `id`  int(10) NOT NULL AUTO_INCREMENT ,
  `friendly`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '404' ,
  `title`  varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Header of page' ,
  `description`  text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT 'Content of page' ,
  PRIMARY KEY (`id`)
);

-- Fill table
INSERT INTO `pages` (`id`, `friendly`, `title`, `description`)
VALUES (1, '?id=1', 'First page', 'Some content on the First page'),
  (2, '?id=2', 'Second page', 'Some content on the Second page'),
  (3, '?id=3', 'Third page', 'Some content on the Third page');
