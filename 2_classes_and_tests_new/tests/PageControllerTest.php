<?php
/**
 * PageControllerTest.php code file.
 * User: Dmitry Sydorenko (sidorenko4er@gmail.com)
 * Date: 27.10.2017
 */

use Classes\Controller\PageController;

/**
 * Class PageControllerTest
 * @group Page
 */
class PageControllerTest extends PHPUnit_Framework_TestCase
{

	/**
	 * @var null|PageController
	 */
	protected static $instance = null;

	public function testClassExists()
	{
		self::$instance = new PageController();
		$this->assertInstanceOf('Classes\\Controller\\PageController', self::$instance);
	}

	public function testEmptyGet()
	{
		$res = self::$instance->getPage();
		$this->assertNotEmpty($res);
		$array = json_decode($res, true);
		$this->assertFalse($array['success']);
		$this->assertEmpty($array['data']);
		$this->assertNotEmpty($array['error']);
		$this->assertEquals("Missed GET param 'id'", $array['error']);
	}

	/**
	 * @param $pageID
	 * @dataProvider dataPageID
	 */
	public function testParams($pageID)
	{
		$res = self::$instance->getPage($pageID);
		$this->assertNotEmpty($res);
		$array = json_decode($res, true);
		$this->assertTrue($array['success']);
		$this->assertNotEmpty($array['data']);
		$this->assertEmpty($array['error']);
		$is_array = is_array($array['data']);
		$this->assertTrue($is_array);
	}

	/**
	 * @return array
	 */
	public function dataPageID()
	{
		return array(
			'test1' => array(1),
			'test2' => array(2),
			'test3' => array(3),
			//'failed_test' => array(4), // Uncomment for fail test
		);
	}

}
