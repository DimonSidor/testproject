<?php
/**
 * sectask.php code file.
 * User: Dmitry Sydorenko (sidorenko4er@gmail.com)
 * Date: 25.10.2017
 */

// include composer dependencies
require_once '../vendor/autoload.php';

$pageController = new \Classes\Controller\PageController();


// Next 10 rows just for visualization test menu
$menu_data = json_decode($pageController->getPages(), true);
if(empty($menu_data['data']) && !empty($menu_data['error']))
{
	print_r($menu_data['error']);
}
elseif(!empty($menu_data['data']) && is_array($menu_data))
{
	$menu = create_test_menu($menu_data['data']);
	print_r($menu);
}


$res = $pageController->getPage();

var_export($res);


/**
 * Create test menu for visualization
 * @param array $data
 * @return string
 */
function create_test_menu(array $data = array()):string
{
	$menu = '';
	if(empty($data) || !is_array($data)) return $menu;

	$menu .= '<ul id="menu-list" ">';
	$menu .= '<li class="menu-li"><a href="/sectask.php">Empty Page</a></li>';
	foreach($data as $page_data)
	{
		if(empty($page_data['friendly']) || empty($page_data['title'])) continue;
		$menu .= '<li class="menu-li"><a href="' . $page_data['friendly'] . '">' . $page_data['title'] . '</a></li>';
	}
	$menu .= '</ul>';

	return $menu;
}
