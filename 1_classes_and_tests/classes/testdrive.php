<?php
/**
 * testdrive.php code file.
 * User: Dmitry Sydorenko (sidorenko4er@gmail.com)
 * Date: 16.10.2017
 */

namespace Classes;

use Exception,
	Google_Client,
	Google_Service_Drive,
	Google_Service_Drive_DriveFile;

class TestDrive
{

	protected $month_ago;

	function __construct()
	{
		session_start();
	}

	/**
	 * @return Google_Client|null
	 */
	function getClient():Google_Client
	{
		$client = null;
		try
		{
			$client = new Google_Client();
			$client->setAuthConfig('../config/oauth_data.json');

			$client->setAccessType("offline"); // offline access

			$client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/');
			$client->addScope(Google_Service_Drive::DRIVE_METADATA_READONLY);

			if(isset($_SESSION['access_token']) && $_SESSION['access_token'])
			{
				$client->setAccessToken($_SESSION['access_token']);
				return $client;
			}
			else
			{
				if(!isset($_GET['code']))
				{
					$auth_url = $client->createAuthUrl();
					header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
				}
				else
				{
					$client->authenticate($_GET['code']);
					$_SESSION['access_token'] = $client->getAccessToken();
					$redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/';
					header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
				}
			}
			return $client;
		}
		catch (Exception $ex)
		{
			// Do something with Exception (send email or write log)
			// print_r($ex->getMessage());
			return $client;
		}
	}

	/**
	 * @param Google_Client|null $client
	 * @return array
	 */
	function getFileList(Google_Client $client = null):array
	{
		$array_files = array();
		if(!($client instanceof Google_Client)) return $array_files;

		try
		{
			$drive = new Google_Service_Drive($client);
			$files = $this->retrieveAllFiles($drive);

			if(empty($files) || !is_array($files))
			{
				return $array_files;
			}

			foreach($files as $file)
			{
				if(!($file instanceof Google_Service_Drive_DriveFile)) continue;

				$array_files[] = array(
					//'id'           => $file->id,
					'Title'         => $file->name,
					//'mimeType'     => $file->mimeType,
					//'description'  => $file->description,
					//'createdDate'  => $file->createdTime,
					'Modified Date' => $file->modifiedTime,
				);
			}
			return $array_files;
		}
		catch (Exception $ex)
		{
			// Do something with Exception (send email or write log)
			// print_r($ex->getMessage());
			return $array_files;
		}
	}

	/**
	 * Retrieve a list of File resources.
	 *
	 * @param Google_Service_Drive $service Drive API service instance.
	 * @return array List of Google_Service_Drive_DriveFile resources.
	 */
	protected function retrieveAllFiles(Google_Service_Drive $service):array
	{
		$result = array();
		$pageToken = null;
		$this->month_ago = date("Y-m-d", strtotime("-1 month"));

		do
		{
			try
			{
				$parameters = array(
					'q'      => "modifiedTime>'{$this->month_ago}'",
					'fields' => 'nextPageToken, files(id, name, mimeType, description, createdTime, modifiedTime)',
				);
				if($pageToken)
				{
					$parameters['pageToken'] = $pageToken;
				}
				$files = $service->files->listFiles($parameters);

				$result = array_merge($result, $files->getFiles());
				$pageToken = $files->getNextPageToken();
			}
			catch(Exception $e)
			{
				if(stripos($e->getMessage(), 'Invalid Credentials') !== false)
				{
					session_destroy();
					$redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/';
					header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
				}
				print "An error occurred: " . $e->getMessage();
				$pageToken = null;
			}
		}
		while($pageToken);

		return $result;
	}

	/**
	 * @param array $doc_data
	 * @return string
	 */
	public function createDocTable(array $doc_data = array()):string
	{
		$doc_table = '';
		if(empty($doc_data) || !is_array($doc_data)) return $doc_table;
		$tb_header = implode('</th><th style=\'border: 1px solid black;\'>', array_keys(current($doc_data)));
		$doc_table .= "<h3>This is Docs table (modified after '{$this->month_ago}'):</h3>";
		$doc_table .= "<table style='border: 1px solid black;'>
			<thead>
				<tr>
					<th style='border: 1px solid black;'>$tb_header</th>
				</tr>
			</thead>
			<tbody>";
		foreach($doc_data as $doc_item)
		{
			$tb_product = implode('</td><td style=\'border: 1px solid black;\'>', $doc_item);
			$doc_table .= "<tr>
					<td style='border: 1px solid black;'>$tb_product</td>
				</tr>";
		}
		$doc_table .= '</tbody>
			</table>';
		return $doc_table;
	}

}