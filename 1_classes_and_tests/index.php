<?php
/**
 * index.php code file.
 * User: Dmitry Sydorenko (sidorenko4er@gmail.com)
 * Date: 16.10.2017
 */

// include composer dependencies
require_once '../vendor/autoload.php';

$testDrive = new \Classes\TestDrive();

$client = $testDrive->getClient();

$files = $testDrive->getFileList($client);

$doc_table = $testDrive->createDocTable($files);

print_r($doc_table);