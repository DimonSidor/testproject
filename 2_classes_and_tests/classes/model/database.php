<?php
/**
 * database.php code file.
 * User: Dmitry Sydorenko (sidorenko4er@gmail.com)
 * Date: 16.10.2017
 */

namespace Classes\Model;

use PDO;
use Exception;

class Database
{

	/**
	 * @var null|PDO
	 */
	private $instance = null;

	/**
	 * @var null|string
	 */
	public $error_msg = null;

	const DB_HOST_DOCKER = 'testproject_mysql_1'; // Change to mysql container name if you have different
	const DB_HOST_TESTS = '192.168.99.100:3306';
	const DB_PORT = '3306';
	const DB_NAME = 'dev_test';
	const DB_USER = 'testuser';
	const DB_PASSWORD = '0000';

	private $db_host = self::DB_HOST_DOCKER;

	public function change_db_host_for_tests()
	{
		$this->db_host = self::DB_HOST_TESTS;
		$this->connect();
	}

	public function __construct()
	{
		if(!isset($this->instance))
		{
			$this->connect();
		}
	}

	protected function connect()
	{
		try
		{
			$this->instance = new PDO('mysql:host=' . $this->db_host . ';port= ' . self::DB_PORT .
					';dbname=' . self::DB_NAME . ';charset=utf8', self::DB_USER, self::DB_PASSWORD);
			$this->instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch (Exception $e)
		{
			$this->error_msg = 'Check const DB_HOST<br>' . $e->getMessage();
		}
	}

	/**
	 * @param string $table_name
	 * @return array
	 */
	public function get_all_from_table($table_name = '')
	{
		$data = array();
		if(empty($table_name)) return $data;
		$sql = "SELECT * FROM $table_name;";
		$res = $this->execute($sql);
		if(empty($res['data']) || !is_array($res['data'])) return $data;
		return $res['data'];
	}

	/**
	 * @param string $table
	 * @param string $where_field
	 * @param string $where_value
	 * @return array|mixed
	 */
	public function get_from_table_where($table = '', $where_field = '', $where_value = '')
	{
		$data = array();
		if(empty($table) || empty($where_field) || !isset($where_value)) return $data;
		$sql = "SELECT * FROM $table WHERE $where_field = '$where_value';";
		$res = $this->execute($sql);
		if(empty($res['data']) || !is_array($res['data'])) return $data;
		return $res['data'];
	}

	/**
	 * @param string $sql
	 * @param bool $need_transaction
	 * @param string $bind_key
	 * @param null $bind_value
	 * @return array
	 */
	private function execute($sql = '', $need_transaction = false, $bind_key = '', $bind_value = null)
	{
		$data = array();
		$return = array(
			'success' => true,
			'sql' => $sql,
			'data' => $data,
			'error' => ''
		);

		// No transaction needed
		if($need_transaction === false)
		{
			try
			{
				$pdo_statement = $this->instance->query($sql);
				$result = $pdo_statement->fetchAll(PDO::FETCH_ASSOC);

				if(empty($result)) return $return;

				$return['data'] = $result;
				return $return;
			}
			catch(Exception $e)
			{
				$this->error_msg = $e->getMessage();
				// Report errors
				$return['success'] = false;
				$return['error'] = $this->error_msg;
				return $return;
			}
		}

		// Execute with transaction
		try
		{
			$this->instance->beginTransaction();

			// Prepare the statements
			$prepare_sql = $this->instance->prepare($sql);

			// Bind values
			if(!empty($bind_key) && !is_null($bind_value))
			{
				$prepare_sql->bindValue(":$bind_key", $bind_value); // , PDO::PARAM_INT
			}
			// Execute
			$prepare_sql->execute();

			$this->instance->commit();
			$return['sql'] = $prepare_sql->queryString;
			return $return;
		}
		catch(Exception $e)
		{
			if($need_transaction === true) $this->instance->rollBack();
			$this->error_msg = $e->getMessage();
			// Report errors
			$return['success'] = false;
			$return['error'] = $this->error_msg;
			return $return;
		}
	}

}