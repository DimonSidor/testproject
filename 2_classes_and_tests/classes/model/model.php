<?php
/**
 * model.php code file.
 * User: Dmitry Sydorenko (sidorenko4er@gmail.com)
 * Date: 16.10.2017
 */

namespace Classes\Model;

use Exception;
use Classes\Model\Database;

class Model
{

	/**
	 * @var string
	 */
	public $def_header = 'Default Page';
	/**
	 * @var string
	 */
	public $header;
	/**
	 * @var string
	 */
	public $content;

	/**
	 * @var Database
	 */
	protected $database;

	/**
	 * @var null|string
	 */
	public $error_msg = null;

	const TEMPLATE_DIR = '/classes/view/templates/';

	protected $vars = array();

	public function __construct()
	{
		$database = new Database();
		$this->database = $database;
		$this->header = $this->def_header;
		$this->content = "<h1>Current page: '{$this->header}'</h1>";
	}

	public function before_run_tests()
	{
		$this->database->change_db_host_for_tests();
	}

	/**
	 * @return bool
	 */
	public function set_content()
	{
		$get = $_GET;
		if(empty($get['id'])) return false;

		$page_data = $this->get_page_data($get['id']);
		if(empty($page_data) || empty($page_data['title']) || empty($page_data['description'])) return false;

		$this->header = $page_data['title'];
		$this->content = $page_data['description'];
		return true;
	}

	/**
	 * @param $template_file
	 * @return mixed|string
	 * @throws Exception
	 */
	protected function render($template_file)
	{
		$content = '';
		if(file_exists(realpath(null) . self::TEMPLATE_DIR . $template_file))
		{
			$content = include realpath(null) . self::TEMPLATE_DIR . $template_file;
		}
		else
		{
			$this->error_msg .= '<br>No template file ' . $template_file . ' present in directory ' . self::TEMPLATE_DIR;
		}
		return $content;
	}

	/**
	 * @param string $title
	 * @return string
	 */
	public function get_header($title = 'Default Header')
	{
		$this->vars['title'] = $title;
		$header = $this->render('header.php');
		return $header;
	}

	/**
	 * @param array $menu_data
	 * @return string
	 */
	public function get_menu($menu_data = array())
	{
		$this->vars['menu_data'] = $menu_data;
		$menu = $this->render('menu.php');
		return $menu;
	}

	/**
	 * @param string $error_msg
	 * @return string
	 */
	public function get_err_msg($error_msg = '')
	{
		$this->vars['error_msg'] = $error_msg;
		$error_message_block = $this->render('error_msg.php');
		return $error_message_block;
	}

	/**
	 * @return string
	 */
	public function get_footer()
	{
		$footer = $this->render('footer.php');
		return $footer;
	}

	/**
	 * @return array
	 */
	public function get_menu_data()
	{
		$data = $this->database->get_all_from_table('pages');
		if(!empty($this->database->error_msg)) $this->error_msg = $this->database->error_msg;
		return $data;
	}

	/**
	 * @param $action
	 * @return array|mixed
	 */
	public function get_page_data($id)
	{
		$data = array();
		$page_data = $this->database->get_from_table_where('pages', 'id', $id);
		if(is_array($page_data) && count($page_data) == 1)
		{
			$key = current(array_keys($page_data));
			$data = $page_data[$key];
		}
		return $data;
	}

}