<?php
/**
 * menu.php code file.
 * User: Dmitry Sydorenko (sidorenko4er@gmail.com)
 * Date: 16.10.2017
 */
$menu = '';
if(empty($this->vars['menu_data']) || !is_array($this->vars['menu_data'])) return $menu;
$menu .= '<ul id="menu-list" ">';
$menu .= '<li class="menu-li">|<a href="/secondtask.php">' . $this->def_header . '</a>|</li>';
foreach($this->vars['menu_data'] as $menu_item)
{
	$menu .= '<li class="menu-li">| <a href="' . $menu_item['friendly'] . '">' . $menu_item['title'] . '</a> |</li>';
}
$menu .= '</ul><div class="separator"></div>';
return $menu;