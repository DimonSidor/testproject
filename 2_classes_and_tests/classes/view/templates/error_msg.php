<?php
/**
 * error_msg.php code file.
 * User: Dmitry Sydorenko (sidorenko4er@gmail.com)
 * Date: 16.10.2017
 */
$error_message_block = '';
if(empty($this->vars['error_msg'])) return $error_message_block;
$error_message_block = "<div class='error-message'>{$this->vars['error_msg']}</div>";
return $error_message_block;