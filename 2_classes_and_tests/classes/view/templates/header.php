<?php
/**
 * header.php code file.
 * User: Dmitry Sydorenko (sidorenko4er@gmail.com)
 * Date: 16.10.2017
 */
return "<!DOCTYPE html>
			<html lang=\"en\">
			<head>
				<title>{$this->vars['title']}</title>
				<style>
					.error-message {
						font-family:Arial, Helvetica, sans-serif;
						font-size:13px;
						border: 1px solid;
						margin: 10px 0px;
						padding:15px 10px 15px 50px;
						background-repeat: no-repeat;
						background-position: 10px center;
						color: #D8000C;
						background-color: #FFBABA;
					}
					table, th, td {
						border: 1px solid black;
					}
					.separator {
						height: 50px;
					}
					#menu-list {
						list-style-type: none;
				    margin: 10px;
				    padding: 0;
					}
					.menu-li {
						display: inline;
					}
				</style>
			</head>
			<body><div class=\"separator\"></div>";