<?php
/**
 * controller.php code file.
 * User: Dmitry Sydorenko (sidorenko4er@gmail.com)
 * Date: 16.10.2017
 */

namespace Classes\Controller;

use Classes\Model\Model;

class Controller
{

	/**
	 * @var Model
	 */
	public $model;

	public function __construct()
	{
		$model = new Model();
		$this->model = $model;
	}

	/**
	 * @return string
	 */
	public function get_page()
	{
		$page = '';
		$this->model->set_content();
		// Header
		$page .= $this->model->get_header($this->model->header);
		// Menu
		$menu_data = $this->model->get_menu_data();
		$page .= $this->model->get_menu($menu_data);
		// Error message
		if(!empty($this->error_msg))
		{
			$page .= $this->model->get_err_msg($this->error_msg);
		}
		// Content
		$page .= $this->model->content;
		// Footer
		$page .= $this->model->get_footer();
		return $page;
	}

	public function before_run_tests()
	{
		$this->model->before_run_tests();
	}

}