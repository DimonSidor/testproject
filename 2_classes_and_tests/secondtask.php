<?php
/**
 * secondtask.php code file.
 * User: Dmitry Sydorenko (sidorenko4er@gmail.com)
 * Date: 16.10.2017
 */

// include composer dependencies
require_once '../vendor/autoload.php';

$controller = new \Classes\Controller\Controller();

//$controller->before_run_tests(); // for local testing on VirtualHost

$page = $controller->get_page();

print_r($page);