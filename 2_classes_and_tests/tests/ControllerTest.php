<?php
/**
 * ControllerTest.php code file.
 * User: Dmitry Sydorenko (sidorenko4er@gmail.com)
 * Date: 16.10.2017
 */

use Classes\Controller\Controller;

/**
 * Class ControllerTest
 * @group Controller
 */
class ControllerTest extends PHPUnit_Framework_TestCase
{

	/**
	 * @var Controller|null
	 */
	private static $controller_instance = null;

	/**
	 * Create all that need for tests
	 */
	public static function setUpBeforeClass()
	{
		self::$controller_instance = new \Classes\Controller\Controller();

		self::$controller_instance->before_run_tests();
	}

	/**
	 * Clear all after finish tests
	 */
	public static function tearDownAfterClass()
	{
		self::$controller_instance = null;
	}

	public function testGetPage()
	{
		$this->assertNotEmpty(self::$controller_instance->get_page());
	}

	public function testSetContent()
	{
		$this->assertFalse(self::$controller_instance->model->set_content());
	}

	public function testGetMenuData()
	{
		$this->assertNotEmpty(self::$controller_instance->model->get_menu_data());
	}

}
